pandoc --template=default.latex -o TP.pdf --variable documentclass=article --variable fontsize=20pt --number-sections --latex-engine=xelatex --highlight-style tango TP.md
pandoc --template=default.latex -o TP-BW.pdf --variable documentclass=article --variable fontsize=20pt --number-sections --latex-engine=xelatex --highlight-style monochrome TP.md
